<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About Us';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Read our high school, college, university and Phd Level writing services we give. 
    We are the best online custom essay writers available 24/7.'
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Term Paper Writing, Essay Writing Service, Research Paper writing, 
    Lab report writing, Admission essay writing, Custom Essay Writing, 
    Dissertation writing service, Personal statement writing, Book Reviews and Reports, 
    Business Plan writing, Editing and Proofreading services, Statistical projects'
]);
?>
<div class="container" style="background-color: #fff">
    <h1 class="text-primary" style="text-align: center;"><?= Html::encode($this->title) ?></h1>
    <div class="site-about">
    <p>Many times you are not able to complete your assignment, due to various reasons.
        Sometimes, you might not have the time to do the assignment due to other commitments that you have to attend to.
        Other times you could just be too tired to work on your paper. Do not be worried.
        We,essaypartners.com, will help you get that grade that you’ve always wanted.</p>

    <p>We are a professional company that offers professional essay writing services to customers from across the globe.
        Our essay writers are from native English speaking countries such as the USA, Ireland, Canada, UK and Australia.
        They write the best essays to our customers at a very affordable price.
        With over ten years in the industry,essaypartners.com promises to give the best paper writing service to you.
        Our support representatives are available 24 hours a day to always make sure that they help you whenever you want to order an essay.
        Our writers are trained to write your custom essay following the exact instruction you give them and therefore guaranteeing
        the best quality essays. </p>

    <p>When you urgently need someone to help you complete your essay, and you visit our site, you can
        keep all your worries aside and wait for the essay. With over 200 qualified writers, we can guarantee you that we will deliver
        top quality work on time. We also make sure to check the work for plagiarism since our work I always 100% unique.
        The team of professional
        essay writers is always on standby to work on all orders that you place with us regardless of the deadlines.</p>

    <p>Since its start in 2003,essaypartners.com has completed over 800,000 essays with an average completion rate of 93.4%.
        We strive to maintain high-quality services to our customers who entrust us with their assignments. Our customer reviews
        are a proof that we indeed meet their expectations and that they are always ready to use our service whenever need be.
        Most of the customers have also referred their friends toessaypartners.com for our quality work. Just place an order with us today,
        sit back and relax. We will deliver <strong>100% UNIQUE</strong> and <strong>QUALITY work ON TIME</strong>. Worry no more,essaypartners.com got you covered.</p>

    <p><strong class="text-primary">Vision</strong></p>
    <p>To be the one site that gives clients 100% value for money by offering the best essay writing services across the globe.</p>
    <p><strong class="text-primary">Mission</strong></p>
    <p>Our mission is to make sure that students have enough time to participate in other school activities by offering to help
      them complete their tasks on time while at the same time maintaining high-quality work that will improve their grades.</p>
    <p><strong class="text-primary">Our Core Values</strong></p>
    <p>We are a team that is devoted and guided by the following five core values.</p>
    <ol>
        <li>Quality Work</li>
        <li>100% Uniqueness</li>
        <li>Timely Delivery</li>
        <li>Zero Grammar Errors</li>
        <li>Professional Customer Support</li>
    </ol>
</div>
</div>
