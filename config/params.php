<?php

return [
    'adminEmail' => 'essaypartners@zohomail.com',
    'managerEmail' => 'essaypartners@zohomail.com',
    'supportEssay'=>'Essay Partners Support',
    'supportEmail' => 'essaypartners@zohomail.com',
    'successUrl'=>"https://essaypartners.com/wallet/paypal",
    'cancelUrl'=>"https://essaypartners.com/wallet/cancel?success=false",
    'successUrl2'=>"https://essaypartners.com/wallet/pay",
    'cancelUrl2'=>"https://essaypartners.com/wallet/order-cancel?success=false",
    'clientId'=>'AYmJcHoiMofhmWwIcc88MOl52PFW6IUdnF10v6vuz1bXLoWm9MMTAKIbD7WZbvbEJ4nbBg3Onx0tc995',
    'clientSecret'=>'EAXyxQGfZhGPzbRNdHJWbJPMNFmRf9rpeNXxyErrVPtWcA0jsQKk-otFu_zDybDhFSN9Uqt79lO9GbVd',
    'mode'=> 'live',
    'couponcode'=>'ESSAYPARTNERS',
    'couponamt'=> 0.9,
    'code2'=>'ESSAY25',
    'code2amt'=> 0.75,
    'code3'=>'ESSAY50',
    'code3amt'=> 0.5,
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration'=>3600,
    '1'=> 'https://essaypartners.com',
    '2'=> 'https://essaypartners.com',
    'recaptcha_api_key'=>'6LdUAcYZAAAAAGIgY5a3j2utiDcwb4Xi1mdBI1gf',
    'recaptcha_api_secret'=>'6LdUAcYZAAAAAODBq41sBeHBLP2_3SKo8HnFptb0'
];
