<?php

use yii\db\Migration;

/**
 * Class m200901_041634_system_seeder
 */
class m200901_041634_system_seeder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

//        $seeder = new \tebazil\yii2seeder\Seeder();
//        $generator = $seeder->getGeneratorConfigurator();
//        $faker = $generator->getFakerConfigurator();
//
//        $seeder->table('service')->columns([
//            'id',
//            'name' => $faker->randomElement(['Writing', 'rewriting', 'editing']),
//            'created_at' => Yii::app()->Date->now(),
//            'updated_at' => Yii::app()->Date->now()
//        ])->rowQuantity(3);
//
//        $seeder->table('type')->columns([
//            'id',
//            'name' => $faker->randomElement([
//                'Essay',
//                'Admission/Application Essay',
//                'Argumentative Essay',
//                'Capstone Project',
//                'Casestudy',
//                'Math Problem'
//            ]),
//            'created_at' => Yii::app()->Date->now(),
//            'updated_at' => Yii::app()->Date->now()
//        ])->rowQuantity(6);
//
//        $password = 'secret';
//
//        $seeder->table('user')->columns([
//            'id',
//            'email' => $faker->email,
//            'username' => $faker->userName,
//            'phone' => $faker->phoneNumber,
//            'access_token' => $faker->randomAscii,
//            'password_hash' => Yii::app()->getSecurity()->generatePasswordHash($password),
//            'auth_key' => $faker->randomAscii,
//            'status' => 1,
//            'site_code' => 'Epat',
//            'country_id' => 1,
//            'timezone' => 'Africa/Nairobi',
//            'blocked_at' => null,
//            'registration_ip' => $faker->ipv4,
//            'confirmed_at' => $faker->dateTime,
//            'confirmed' => 1,
//            'created_at' => $faker->dateTime,
//            'updated_at' => $faker->dateTime
//        ]);
//



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200901_041634_system_seeder cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200901_041634_system_seeder cannot be reverted.\n";

        return false;
    }
    */
}
