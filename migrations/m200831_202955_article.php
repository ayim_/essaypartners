<?php

use yii\db\Migration;

/**
 * Class m200831_202955_article
 */
class m200831_202955_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'footer' => $this->string(),
            'blog' => $this->string(),
            'keywords' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_202955_article cannot be reverted.\n";

        return false;
    }
    */
}
