<?php

use yii\db\Migration;

/**
 * Class m200831_203603_frontorder
 */
class m200831_203603_frontorder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('frontorder', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->unsigned(),
            'type_id' => $this->integer()->unsigned(),
            'pages_id' => $this->integer()->unsigned(),
            'level_id' => $this->integer()->unsigned(),
            'urgency_id' => $this->integer()->unsigned(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('frontorder');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203603_frontorder cannot be reverted.\n";

        return false;
    }
    */
}
