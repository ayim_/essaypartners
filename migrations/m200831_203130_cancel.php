<?php

use yii\db\Migration;

/**
 * Class m200831_203130_cancel
 */
class m200831_203130_cancel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cancel', [
            'id' => $this->primaryKey(),
            'order_number' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cancel');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203130_cancel cannot be reverted.\n";

        return false;
    }
    */
}
