<?php

use yii\db\Migration;

/**
 * Class m200831_204339_style
 */
class m200831_204339_style extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned(),
            'order_id' => $this->integer()->unsigned(),
            'file_date' => $this->integer(),
            'attached' => $this->string(),
            'file_extension' =>$this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('style');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204339_style cannot be reverted.\n";

        return false;
    }
    */
}
