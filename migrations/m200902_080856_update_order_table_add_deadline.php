<?php

use yii\db\Migration;

/**
 * Class m200902_080856_update_order_table_add_deadline
 */
class m200902_080856_update_order_table_add_deadline extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'deadline', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'deadline');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_080856_update_order_table_add_deadline cannot be reverted.\n";

        return false;
    }
    */
}
