<?php

use yii\db\Migration;

/**
 * Class m200831_203839_message
 */
class m200831_203839_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'general' => $this->integer(),
            'status' => $this->integer(),
            'title' => $this->string(),
            'sender_id' => $this->bigInteger()->unsigned(),
            'receiver_id' => $this->bigInteger()->unsigned(),
            'order_number' => $this->integer(),
            'message' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203839_message cannot be reverted.\n";

        return false;
    }
    */
}
