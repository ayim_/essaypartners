<?php

use yii\db\Migration;

/**
 * Class m200831_203015_auth
 */
class m200831_203015_auth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auth', [
            'id' => $this->string(),
            'user_id' => $this->bigInteger()->unsigned(),
            'source' => $this->string(),
            'source_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('auth');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203015_auth cannot be reverted.\n";

        return false;
    }
    */
}
