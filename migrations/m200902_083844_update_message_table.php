<?php

use yii\db\Migration;

/**
 * Class m200902_083844_update_message_table
 */
class m200902_083844_update_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('message', 'context', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('message', 'context');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_083844_update_message_table cannot be reverted.\n";

        return false;
    }
    */
}
