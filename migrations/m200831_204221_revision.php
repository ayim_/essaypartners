<?php

use yii\db\Migration;

/**
 * Class m200831_204221_revision
 */
class m200831_204221_revision extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('revision', [
            'id' => $this->primaryKey(),
            'order_number' => $this->integer(),
            'instructions' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('revision');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204221_revision cannot be reverted.\n";

        return false;
    }
    */
}
