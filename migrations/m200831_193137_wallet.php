<?php

use yii\db\Migration;

/**
 * Class m200831_193137_wallet
 */
class m200831_193137_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wallet', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->bigInteger()->unsigned(),
            'order_id' => $this->bigInteger()->unsigned(),
            'narrative' => $this->string(),
            'deposit' => $this->float(),
            'withdraw' => $this->float(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('wallet');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_193137_wallet cannot be reverted.\n";

        return false;
    }
    */
}
