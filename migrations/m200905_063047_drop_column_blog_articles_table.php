<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%column_blog_articles}}`.
 */
class m200905_063047_drop_column_blog_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article', 'blog', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('article', 'blog');
    }
}
