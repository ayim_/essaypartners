<?php

use yii\db\Migration;

/**
 * Class m200831_204120_rating
 */
class m200831_204120_rating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'value' => $this->double(),
            'client_id' => $this->integer()->unsigned(),
            'order_number' => $this->integer(),
            'description' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rating');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204120_rating cannot be reverted.\n";

        return false;
    }
    */
}
