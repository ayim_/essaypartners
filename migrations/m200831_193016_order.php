<?php

use yii\db\Migration;

/**
 * Class m200831_193016_order
 */
class m200831_193016_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'service_id' => $this->bigInteger()->unsigned(),
            'cancelled' => $this->integer(),
            'ordernumber' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'written_by' => $this->integer(),
            'edited_by' => $this->integer(),
            'type_id' => $this->bigInteger()->unsigned(),
            'urgency_id' => $this->bigInteger()->unsigned(),
            'spacing_id' => $this->bigInteger()->unsigned(),
            'style_id' => $this->bigInteger()->unsigned(),
            'sources_id' => \yii\db\Schema::TYPE_INTEGER,
            'language_id' => \yii\db\Schema::TYPE_INTEGER,
            'pagesummary' => \yii\db\Schema::TYPE_INTEGER,
            'plagreport' => \yii\db\Schema::TYPE_INTEGER,
            'initial_draft' => \yii\db\Schema::TYPE_INTEGER,
            'qualitycheck' => \yii\db\Schema::TYPE_INTEGER,
            'topwriter' => \yii\db\Schema::TYPE_INTEGER,
            'instructions' => \yii\db\Schema::TYPE_STRING,
            'active' => \yii\db\Schema::TYPE_BOOLEAN,
            'paid' => \yii\db\Schema::TYPE_BOOLEAN,
            'completed' => \yii\db\Schema::TYPE_BOOLEAN,
            'disputed' => \yii\db\Schema::TYPE_BOOLEAN,
            'approved' => \yii\db\Schema::TYPE_BOOLEAN,
            'rejected' => \yii\db\Schema::TYPE_BOOLEAN,
            'editing' => \yii\db\Schema::TYPE_BOOLEAN,
            'revision' => \yii\db\Schema::TYPE_BOOLEAN,
            'available' => \yii\db\Schema::TYPE_BOOLEAN,
            'confirmed' => \yii\db\Schema::TYPE_BOOLEAN,
            'phone' => \yii\db\Schema::TYPE_STRING,
            'amount' => \yii\db\Schema::TYPE_FLOAT,
            'topic' => \yii\db\Schema::TYPE_STRING,
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_193016_order cannot be reverted.\n";

        return false;
    }
    */
}
