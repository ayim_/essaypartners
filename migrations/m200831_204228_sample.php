<?php

use yii\db\Migration;

/**
 * Class m200831_204228_sample
 */
class m200831_204228_sample extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sample', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'level_id' => $this->integer(),
            'subject_id' => $this->integer(),
            'page_id' => $this->integer(),
            'source_id' => $this->integer(),
            'style_id' => $this->integer(),
            'type_id' => $this->integer(),
            'icon' => $this->string(),
            'photo' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('sample');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204228_sample cannot be reverted.\n";

        return false;
    }
    */
}
