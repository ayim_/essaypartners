<?php

use yii\db\Migration;

/**
 * Class m200831_203952_paypal
 */
class m200831_203952_paypal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('paypal', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned(),
            'payment_id' => $this->integer()->unsigned(),
            'order_number' => $this->integer(),
            'amount_paid' => $this->float(),
            'hash' => $this->string(),
            'withdraw' => $this->float(),
            'complete' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('paypal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203952_paypal cannot be reverted.\n";

        return false;
    }
    */
}
