<?php

use yii\db\Migration;

/**
 * Class m200831_193321_user_profile
 */
class m200831_193321_user_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_profile', [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->unsigned(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'city' => $this->string(),
            'phone' => $this->string(),
            'country' => $this->string(),
            'gender' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_193321_user_profile cannot be reverted.\n";

        return false;
    }
    */
}
