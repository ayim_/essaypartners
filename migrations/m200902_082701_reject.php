<?php

use yii\db\Migration;

/**
 * Class m200902_082701_reject
 */
class m200902_082701_reject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reject', [
            'id' => $this->primaryKey(),
            'order_number' => $this->integer(),
            'reason_id' => $this->integer(),
            'description' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('reason');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_082701_reject cannot be reverted.\n";

        return false;
    }
    */
}
