<?php

use yii\db\Migration;

/**
 * Class m200831_204140_reason
 */
class m200831_204140_reason extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200831_204140_reason cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204140_reason cannot be reverted.\n";

        return false;
    }
    */
}
