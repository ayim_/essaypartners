<?php

use yii\db\Migration;

/**
 * Class m200831_203851_notification
 */
class m200831_203851_notification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'key_id' => $this->string(),
            'type' => $this->string(),
            'user_id' => $this->integer()->unsigned(),
            'order_number' => $this->integer(),
            'seen' => $this->integer(),
            'flashed' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notification');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203851_notification cannot be reverted.\n";

        return false;
    }
    */
}
