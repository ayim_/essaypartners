<?php

use yii\db\Migration;

/**
 * Class m200831_203914_page
 */
class m200831_203914_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'source_id' => $this->integer()->unsigned(),
            'space_id' => $this->integer(),
            'no_of_pages' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_203914_page cannot be reverted.\n";

        return false;
    }
    */
}
