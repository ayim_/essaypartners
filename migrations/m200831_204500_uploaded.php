<?php

use yii\db\Migration;

/**
 * Class m200831_204500_uploaded
 */
class m200831_204500_uploaded extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('uploaded', [
            'id' => $this->primaryKey(),
            'order_number' => $this->integer(),
            'writer_id' => $this->integer(),
            'file_type' => $this->integer(),
            'download_date' => $this->string(),
            'file_extension' => $this->string(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('uploaded');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204500_uploaded cannot be reverted.\n";

        return false;
    }
    */
}
