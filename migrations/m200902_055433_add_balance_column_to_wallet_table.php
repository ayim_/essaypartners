<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%wallet}}`.
 */
class m200902_055433_add_balance_column_to_wallet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('wallet', 'balance', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('wallet', 'balance');
    }
}
