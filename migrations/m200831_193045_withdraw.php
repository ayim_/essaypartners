<?php

use yii\db\Migration;

/**
 * Class m200831_193045_withdraw
 */
class m200831_193045_withdraw extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('withdraw', [
            'id' => $this->primaryKey(),
            'status' => $this->integer(),
            'user_id' => $this->bigInteger()->unsigned(),
            'uniqueid' => $this->bigInteger(),
            'amount' => $this->float(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('withdraw');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_193045_withdraw cannot be reverted.\n";

        return false;
    }
    */
}
