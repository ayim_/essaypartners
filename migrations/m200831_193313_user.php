<?php

use yii\db\Migration;

/**
 * Class m200831_193313_user
 */
class m200831_193313_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'username' => $this->string(),
            'phone' => $this->string(),
            'access_token' => $this->string(),
            'password_hash' => $this->string(),
            'auth_key' => $this->string(),
            'password_reset_token' => $this->string(),
            'status' => $this->integer(),
            'site_code' => $this->integer(),
            'country_id' => $this->bigInteger()->unsigned(),
            'timezone' => $this->string(),
            'blocked_at' => $this->string(),
            'registration_ip' => $this->string(),
            'confirmed_at' => $this->string(),
            'confirmed' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }

}
