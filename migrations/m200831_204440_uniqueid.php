<?php

use yii\db\Migration;

/**
 * Class m200831_204440_uniqueid
 */
class m200831_204440_uniqueid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('uniqueid', [
            'id' => $this->primaryKey(),
            'orderId' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('uniqueid');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_204440_uniqueid cannot be reverted.\n";

        return false;
    }
    */
}
